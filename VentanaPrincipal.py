from sqlite3 import connect
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gio
import os


# se crea la Ventana Principal
class VentanaPrincipal(Gtk.Window):
    def __init__(self):
        #se agrega Titulo
        super().__init__(title="VentanaSalvadora")
        #borde
        self.set_border_width(50)
        #se agrega parametros
        self.set_size_request(920,920)
        #genera Minilizacion y maximizacion
        self.set_resizable(True)
        
        
        #Entry
        #se crean entradas de texto
        self.ent_entry = Gtk.Entry()
        #placeholder hace que las letras sean en gris
        self.ent_entry.set_placeholder_text("Entrada 1")
        self.ent_entry2 = Gtk.Entry()
        self.ent_entry2.set_placeholder_text("Entrada 2")
        #Idenficar cada entrada
        text1 = self.ent_entry.get_text()
        text2 = self.ent_entry2.get_text()

        
        #labels

        self.lbl_info1= Gtk.Label()
        self.lbl_info1.set_text("Ingrese texto:")
        self.lbl_info2=Gtk.Label()
        self.lbl_info2.set_text("Ingrese text:")
        
        self.lbl_result= Gtk.Label()
        self.lbl_result.set_text("Resultado:")

        #botones
        self.btn_aceptar = Gtk.Button(label="Aceptar")
        self.btn_aceptar.connect("clicked", self.aceptar)

        self.btn_guardar=Gtk.Button(label="Guardar")
        self.btn_guardar.connect("clicked",self.guardar)

        self.btn_reiniciar=Gtk.Button(label="Reiniciar")
        self.btn_reiniciar.connect("clicked",self.reiniciar)

        #Organization dentro de la Ventana
        grid = Gtk.Grid()
        grid.set_column_spacing(110)
        grid.set_row_spacing(30)
        grid.attach(self.lbl_info2,0,0,4,1)
        grid.attach(self.ent_entry, 0, 1, 5, 3)
        grid.attach(self.lbl_info1, 2,0,9,1)
        grid.attach(self.btn_guardar,2,10,2,1)
        grid.attach(self.lbl_result,2,12,4,1)
        grid.attach_next_to(self.ent_entry2,self.ent_entry,Gtk.PositionType.RIGHT,5,3)
        grid.attach_next_to(self.btn_aceptar,self.btn_guardar,Gtk.PositionType.RIGHT,2,1)
        grid.attach_next_to(self.btn_reiniciar,self.btn_aceptar,Gtk.PositionType.RIGHT,2,1)
        self.add(grid)
    
    #funciones 
    def aceptar(self,widget):
        text1 = self.ent_entry.get_text()
        text2 = self.ent_entry2.get_text()
        dialoginfo = Gtk.MessageDialog(transient_for = self, flags = 0, 
        message_type = Gtk.MessageType.INFO, buttons = Gtk.ButtonsType.OK ,
        text = text1 + " " + text2)
        Respuesta_dialog =dialoginfo.run()
        if Respuesta_dialog == Gtk.ResponseType.OK:
            dialoginfo.destroy()
        
        dialoginfo.destroy()
    
    def guardar(self,widget):
        dialog = Gtk.FileChooserDialog(title="Ventana Guardar", 
        parent=self, action=Gtk.FileChooserAction.SAVE)
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE,
            Gtk.ResponseType.OK,
        )
        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            file = open("pavanzada.txt", "a+")
            text1 = self.ent_entry.get_text()
            text2 = self.ent_entry2.get_text()
            file.write(text1 + text2)
            file.close()
            dialog.destroy()
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
        dialog.destroy()
    
    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

    def reiniciar(self,widget):
        dialogwarning = Gtk.MessageDialog(transient_for = self, flags = 0, 
        message_type = Gtk.MessageType.WARNING, buttons = Gtk.ButtonsType.OK_CANCEL ,
        text = "¿Desea Reiniciar?")
        Respuesta_dialogw =dialogwarning.run()
        if Respuesta_dialogw == Gtk.ResponseType.OK:
            self.ent_entry.set_text("")
            self.ent_entry2.set_text("")
            dialogwarning.destroy()
        elif dialogwarning == Gtk.ResponseType.CANCEL:
            dialogwarning.destroy()
        dialogwarning.destroy()
            
            
winR = VentanaPrincipal()
winR.connect("destroy", Gtk.main_quit)
winR.show_all()
Gtk.main()
